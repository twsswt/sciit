---
@issue add-close-issue-button-to-web
@title Add Close Issue Button to Web Interface
@description 
 Be able to close any issue directly in the user interface on 
 the web application.  This would work by removing the issue text 
 from the containing file comment.  If this left the file empty it 
 should probably also remove the empty file (would perhaps want to
 make this an option?
---
