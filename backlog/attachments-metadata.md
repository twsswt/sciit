---
@issue attachments-metadata
@title Attachments Metadata Item
@description
 Most issue trackers support the ability to attach documents 
 to issues. For sciit, this would just need to be an @attachment
 tag followed by a URL or path.  This path could be to a local 
 repository object or some other storage service.
---

