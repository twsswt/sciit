.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.md

More styles and supported files can be found in :ref:`styles_toplevel`.

Guidance on command line interaction can be found in :ref:`commandline_toplevel`.

Contents
========

.. toctree::
   :maxdepth: 4
   
   API Documentation <api/modules.rst>
   styles.rst
   changelog.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Fork this project
==================

* https://gitlab.com/nystrome/sciit
