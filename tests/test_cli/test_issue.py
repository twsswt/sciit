import sys
from io import StringIO
from unittest import TestCase
from unittest.mock import Mock, patch, MagicMock

from sciit.cli.issue import issue
from tests.test_cli.external_resources import ansi_escape, first_commit, second_commit, issues


class TestIssueCommand(TestCase):

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()
        self.args = Mock()
        self.args.repo = MagicMock()

    def test_command_fails_if_no_issues_matched(self):
        self.args.revision = second_commit.hexsha
        self.args.normal = self.args.detailed = self.args.full = False
        self.args.issue_id = ''

        mock_head = Mock()
        mock_head.commit = second_commit
        mock_head.name = 'master'
        self.args.repo.heads = [mock_head]

        issue(self.args)

        self.assertIn('No issues found matching', sys.stdout.getvalue())
        self.assertIn('Here are issues that are in the tracker:', sys.stdout.getvalue())

    def test_command_returns_no_history(self):
        self.args.revision = first_commit.hexsha
        self.args.normal = self.args.detailed = self.args.full = False
        self.args.issue_id = ''

        self.args.repo.build_history.return_value = {}

        issue(self.args)
        self.assertIn('No issues in the repository', sys.stdout.getvalue())

    @patch('tests.external_resources.IssueSnapshot.in_branches', new_callable=MagicMock(return_value=['master']))
    def test_command_returns_correct_history_normal_view(self, in_branches):

        self.args.revision = second_commit.hexsha
        self.args.normal = True
        self.args.detailed = self.args.full = False
        self.args.issue_id = '12'
        self.args.repo.build_history.return_value = {'12': issues['12']}

        output = issue(self.args)
        output = ansi_escape.sub('', output)

        self.assertIn('ID:                12', output)
        self.assertIn('Status:            Open', output)
        self.assertIn('Description:', output)
        self.assertIn('Latest file path:  path', output)
        self.assertNotIn('IssueSnapshot Revisions:', output)
        self.assertNotIn('Commit Activity:', output)

    def test_command_returns_correct_history_full_view(self):
        self.args.revision = second_commit.hexsha
        self.args.full = True
        self.args.normal = self.args.detailed = False
        self.args.issue_id = '12'
        self.args.repo.build_history.return_value = {'12': issues['12']}

        output = issue(self.args)

        output = ansi_escape.sub('', output)
        self.assertIn('ID:                12', output)
        self.assertIn('Status:            Open', output)
        self.assertIn('Existed in:', output)
        self.assertIn('Present in Commits (1):', output)
        self.assertIn('Revisions to Issue (1):', output)
